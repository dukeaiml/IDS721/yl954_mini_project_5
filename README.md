# yl954_mini_project_5

This is a simple Serverless Rust Microservice. This service will read the list in the database, and sort it.

## Author

Yuanzhi (Matthew) Lou

## Demo

[Live Preview](https://tgysfr5km8.execute-api.us-east-1.amazonaws.com/yl954_mini_project_5)

## Connect Database
1. ```sudo apt-get update```
2. ```sudo apt-get install pkg-config libssl-dev```
3. Add ```mysql_async = "0.32.1"``` in ```Cargo.toml```

## Lambda Functionality

1. This function connects the mysql database named ```mini5db```.
2. This function sorts interger lists
3. There are two parameters you can set: ```order(default:a)``` and ```list(default:[])```
4. For ```order```, ascending when its value is ```a``` and descending with all other values
5. For ```list```, each number is delimited by comma, and spaces have no effect on the result
6. Example:
```
https://tgysfr5km8.execute-api.us-east-1.amazonaws.com/yl954_mini_project_5
```

## Deploy lambda function

1. Go to IAM service, create access key and save the ```access key ID``` and ```secret access key```
2. Create a new role with the policy of ```AWSLambda_FullAccess``` and ```AmazonVPCCrossAccountNetworkInterfaceOperations```
3. In your VM, create the file ```~/.aws/credentials```, and add the following content

```
[default]
aws_access_key_id = your_access_key_ID
aws_secret_access_key = your_secret_access_key
```

5. Under your project: ```cargo lambda deploy --region your_region --iam-role arn:aws:iam::your_account_ID:role/your_role_name```
6. Then you can find the lambda function in your AWS account

## Create Database
1. Open ```RDS``` service, click ```Create database```, choose ```Easy create```, ```MySQL```, and name the database ```mini5db``` 
![](databaseinstance.png)
2. Remember to make sure Publicly accessible is yes and set the inbound rule
3. connect the database instance, create the schema ```testdb```, and create the table ```sort```
4. add data
![](database.png)
