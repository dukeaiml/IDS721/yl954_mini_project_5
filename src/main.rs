use tracing_subscriber::filter::{EnvFilter, LevelFilter};use lambda_http::{run, service_fn, Body, Error, Request, Response};
use serde::{Deserialize, Serialize};
use mysql_async::Pool;
use mysql_async::prelude::Queryable;
use mysql_async::Row;

#[derive(Serialize, Deserialize, Default)]
struct ResData {
    results: Vec<SubResData>,
}

#[derive(Serialize, Deserialize, Default)]
struct SubResData {
    status_message: String,
    order: String,
    original_list: Vec<i32>,
    sorted_list: Vec<i32>,
}

async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    let database_url = "mysql://admin:12345678@mini5db.ct2cuqk4g0xs.us-east-1.rds.amazonaws.com:3306/testdb";
    let pool = Pool::new(database_url);
    let mut conn = pool.get_conn().await?;
    let rows: Vec<Row> = conn.query("SELECT * FROM sort").await?;

    let mut res_data: ResData = Default::default();
    for result in rows {
        let mut sub_res_data: SubResData = Default::default();
        let list: String = result.get("list").expect("Error getting 'list'");
        let trimmed = &list[1..list.len()-1];
        let mut vec: Vec<i32> = trimmed.split(',')
                               .map(|s| s.trim().parse::<i32>())
                               .collect::<Result<Vec<i32>, _>>()
                               .unwrap_or(Vec::new());
        let order_: String = result.get("order").expect("Error getting 'order'");
        let order =  if order_ == "a" {"Ascending"} else {"Descending"};
        sub_res_data.order = order.to_string();
        sub_res_data.original_list = vec.clone();
        vec.sort();
        if order_ != "a" {
            vec.reverse();
        }
        sub_res_data.sorted_list = vec.clone();
        let err_check_str = format!("{:?}", vec);
        if list.len() == err_check_str.len() {
            sub_res_data.status_message = "Success".to_string();
        } else {
            sub_res_data.status_message = "Fail".to_string();
        }
        res_data.results.push(sub_res_data);
    }
    let resp = Response::builder()
    .status(200)
    .header("content-type", "text/html")
    .body(serde_json::to_string(&res_data).unwrap().into())
    .map_err(Box::new)?;
    conn.disconnect().await?;
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .with_target(false)
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
